# Smoelenboek

## Installation

For installation:

* Clone this project
* Import db.sql
* Edit config.inc.php and enter database username and password
* Go to project/assignment/index.php

## Using

Users can create a profile containing the following information:
Username, password, birthday, city, education, email, phone number.

Other users can be found with search, information of these users is displayed after clicking "show profile".
Users can also edit their profile and log out.



