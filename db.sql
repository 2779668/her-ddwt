-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 09, 2018 at 08:32 PM
-- Server version: 5.7.20-0ubuntu0.17.04.1
-- PHP Version: 7.0.22-0ubuntu0.17.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userId` int(11) NOT NULL,
  `Username` varchar(30) NOT NULL,
  `Email` varchar(60) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `bday` varchar(11) NOT NULL,
  `city` varchar(22) NOT NULL,
  `education` varchar(20) NOT NULL,
  `pnumber` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `Username`, `Email`, `Password`, `bday`, `city`, `education`, `pnumber`) VALUES
(48, 'gfg', '', '$2y$10$cmHgodQ7Dq5dmoIOkqtiyuBDs2/zok37oy9HV2Th5i1s7dAwIKT/m', '', '', '', ''),
(54, 'ttttt', 'tttt', '$2y$10$5Q3zZ1IyxYJyHdn0IDyyHu0Y5/mggOtmxJeeAJs2BlszEo49d/UX2', 'tttt', 'tt', 't', 'tt'),
(55, '555555', '555', '$2y$10$GUo.yS/bco4UkCdqFLjf/OhpIctpOY1M3CawsVrpKJgG1paumXjBW', '5555', '555', '555', '555'),
(56, '211221', '2121211', '$2y$10$zlFa7rtrONh/CcNqasWQlO.BhSBgi11u.Rqj9R0vXoPYPSSjQfAQ2', '21212121', '21211', '2121', '212111'),
(62, '456', '456', '$2y$10$m0J9bmX2HJE0fvJySGzKaOGZXpePDiOEPgucYx42/0nMlH9zc2fW2', '44', '444', '44', '44'),
(63, '789', '777', '$2y$10$VL129qvGlmJmlbfuBBUKAuS2uu2bq7aHdjNtt9W0w3/pbWmeuY.qS', '777', '777', '77', '77'),
(66, 'hyy', 'j@gmail.com', '$2y$10$OS1ksvH1nUNwYbOKIJ1t3.5zf8I7tKcV89cFU6Pyd4SJ.A5jHkAmC', 'adsd', 'addsa', 'aasddad', 'a'),
(67, 'hyyu', 'hy', '$2y$10$F2zroMtCnZYnDKK4NJw86O/OkcwG2/UV1th0ptdeh.rkaaZ8Rh5OO', 'hy', 'hy', 'y', 'hyh'),
(68, 'dsfsfd', '4', '$2y$10$VskSjuItu2jLhKO0PpW6J.E1FpZUVGeQ6AMAtb8mBU4.xJHrl/0wS', '4', '4', '44', '4'),
(69, 'adsaa', 'as', '$2y$10$0F9BnZh3CLWRth9/B6wm5e7tuik9or3qB9HhPKgWkigLA9m2NbQQS', 'asd', 'ads', 'asd', 'asd'),
(71, 'abcd', '4@gmail.com', '$2y$10$7xKB4L/4nIOJ.0ypavr3KOHBplhwRefwnJcgoYK6f.L4OvhxV.gN2', '22', '22', '', '123456'),
(72, 'tr', 'tr@gmail.com', '$2y$10$rbZ1mopZMe7J6FPn/wyrvOA03xV9.QCbyGkRjdxwy1Lxc2UvXfPzi', '01-01-1970', 'tr', 'hy', '0636595114'),
(73, 'df', 'df@gmail.com', '$2y$10$.Ii1rm7rdDNBzr.utYp8c.xhjIC6Z4I4o9xHx2YRMS/XaJZe9rOUm', '01-01-1970', 'df', '2', '0636595114');


-- --------------------------------------------------------


--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`),
  ADD UNIQUE KEY `userEmail` (`Email`);



--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
