<?php

require_once('config.inc.php');

require_once('header.php');

try {
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

$searchn = $_POST['searchn'];

$query = "SELECT * FROM users WHERE Username LIKE :searchn";
$stmt = $dbh->prepare($query);
$stmt->bindValue(':searchn', '%' . $searchn . '%', PDO::PARAM_INT);
$stmt->execute();
$results = $stmt->fetchAll();
?>

    <h1>Search</h1>

<?php
foreach ($results as $row) {
    ?>
    <?php echo $row["Username"]; ?>
    <a class="btn btn-success" href="user.php?userId=<?php echo $row['userId']; ?>" role="button">Show profile</a>


    <br />
<?php
}
?>


<?php

require_once('footer.php');

?>