<?php
require_once('config.inc.php');
require_once('header.php');

$name = $bday = $email = $education = $city = $pnumber = '';
$errors = '';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["username"])) {
        $errors .= '<li>Please enter username</li>';
    } else {
        $name = $_POST["username"];
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z0-9]*$/", $name)) {
            $errors .= '<li>Incorrect username: only alphanumeric allowed</li>';
        }
    }

    if (empty($_POST["email"])) {
        $errors .= '<li>Please enter email</li>';
    } else {
        $email = $_POST["email"];
        // check if e-mail address is well-formed
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors .= '<li>Incorrect email</li>';
        }
    }

    if (empty($_POST["pnumber"])) {
        $errors .= '<li>Please enter phone number</li>';
    } else {
        $pnumber = $_POST["pnumber"];

        # htt
        if (!preg_match ('#^0[1-9][0-9]{0,2}-?[1-9][0-9]{5,7}$#', trim($pnumber))) {
            $errors .= '<li>Incorrect phone number</li>';
        }
    }

    $bday = $_POST["bday"];
    $bday2 = explode('-', $bday);
    if (count($bday2) != 3 OR !checkdate($bday2[1], $bday2[0], $bday2[2])) {
        $errors .= '<li>Please enter birthday</li>';
    }

    $education = $_POST["education"];
    $city = $_POST["city"];

    if ($errors == '') {
        $param_password = password_hash($_POST['password'], PASSWORD_DEFAULT);

        $query = $dbh->prepare('INSERT INTO users (Username, Password, Email, bday, city, education, pnumber) VALUES (?,?,?,?,?,?,?)');
        $query->execute(array($_POST['username'], $param_password, $_POST['email'], $_POST['bday'], $_POST['city'], $_POST['education'], $_POST['pnumber']));

        //echo 'Account created';

        header('Location: index.php');
        exit;
    }
}

?>


        <div class="row marketing">
            <div class="col-lg-6">

                <h1>Create user</h1>
                <div class="container">


                    <?php if ($errors != '') { ?>
                        <div class="alert alert-danger" role="alert">

                            <p>Something went wrong:</p>

                            <ul>
                                <?php echo $errors; ?>
                            </ul>
                        </div>
                    <?php } ?>


                    <form id="createForm" action="login.php" method="POST">
                    <h2> Register </h2>
                        <p>Username: </p><input type=text id=username name=username value="<?php echo htmlspecialchars($name); ?>" placeholder="Username"><br />

                        <p>Password: </p><input type=password id=password name="password" placeholder=Password value="<?php echo htmlspecialchars(""); ?>"><br>

                        <p>E-mail: </p><input type=text id=email name=email placeholder=JohnDoe@hotmail.com value="<?php echo htmlspecialchars($email); ?>"><br>
                        <p>City: </p><input type=text id=city name=city placeholder=City value="<?php echo htmlspecialchars($city); ?>"><br>
                        <p>Birthday </p><input type=text id=bday name=bday placeholder=01-01-1970 value="<?php echo htmlspecialchars($bday); ?>"><br>

                        <p>Phone number: </p><input type=text id=pnumber name=pnumber placeholder=0612345678 value="<?php echo htmlspecialchars($pnumber); ?>"><br>
                        <p>Education: </p><input type=text id=education name=education placeholder=Education value="<?php echo htmlspecialchars($education); ?>"><br>


                        <button name="submit" type="submit" class="btn btn-success">Register</button>


                    </form>

                </div>


            </div>

            <div class="col-lg-6">

                <h1>Login</h1>

                <form method="post" action="login_post.php">
                    <div class="form-group1">
                        <label for="exampleInputEmail1">Username</label>
                        <input type="text" name="uname" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Username">
                    </div>
                    <div class="form-group2">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" name="pw" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>

                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </div>

<?php
require_once('footer.php');
?>

