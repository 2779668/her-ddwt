<?php

require_once('config.inc.php');
require_once('header.php');
require_once('password.inc.php');


if (isset($_POST['uname']) && isset($_POST['pw'])) {

    $query = $dbh->prepare(
        'SELECT * FROM users WHERE username = ?'
    );
    $query->execute(array($_POST['uname']));
    $row = $query->fetch();



    if ($row && password_verify($_POST['pw'], $row['Password'])) {
        $_SESSION['user_id'] = $row['userId'];
        header(
            'Location: index.php'
        );
    } else {
        header('Location: login.php');
    }
}

