<?php
require_once('config.inc.php');
require_once('header.php');



$query = $dbh->prepare(
    'SELECT * FROM users WHERE userId = ?'
);
$query->execute(array($_SESSION['user_id']));
$results = $query->fetch();

$name = $results['Username'];
$email = $results['Email'];
$bday = $results['bday'];
$city = $results['city'];
$education = $results['education'];
$pnumber = $results['pnumber'];

$errors = '';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["username"])) {
        $errors .= '<li>Please enter username</li>';
    } else {
        $name = $_POST["username"];
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z0-9]*$/", $name)) {
            $errors .= '<li>Incorrect username: only alphanumeric allowed</li>';
        }
    }

    if (empty($_POST["email"])) {
        $errors .= '<li>Please enter email</li>';
    } else {
        $email = $_POST["email"];
        // check if e-mail address is well-formed
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors .= '<li>Incorrect email</li>';
        }
    }

    if (empty($_POST["pnumber"])) {
        $errors .= '<li>Please enter phone number</li>';
    } else {
        $pnumber = $_POST["pnumber"];

        # htt
        if (!preg_match ('#^0[1-9][0-9]{0,2}-?[1-9][0-9]{5,7}$#', trim($pnumber))) {
            $errors .= '<li>Incorrect phone number</li>';
        }
    }

    $bday = $_POST["bday"];
    $bday2 = explode('-', $bday);
    if (count($bday2) != 3 OR !checkdate($bday2[1], $bday2[0], $bday2[2])) {
        $errors .= '<li>Please enter birthday</li>';
    }

    $education = $_POST["education"];
    $city = $_POST["city"];

    if ($errors == '') {
        $param_password = password_hash($_POST['password'], PASSWORD_DEFAULT);

        $query = $dbh->prepare('UPDATE users SET Username=?, Password=?, Email=?, bday=?, city=?, education=?, pnumber=? WHERE userId=?');
        $query->execute(array($_POST['username'], $param_password, $_POST['email'], $_POST['bday'], $_POST['city'], $_POST['education'], $_POST['pnumber'], $_SESSION['user_id']));

        //echo 'Account created';

        header('Location: index.php');
        exit;
    }
}
?>



<div class="container">

    <h1>Profile</h1>

    <?php if ($errors != '') { ?>
        <div class="alert alert-danger" role="alert">

            <p>Something went wrong:</p>

            <ul>
                <?php echo $errors; ?>
            </ul>
        </div>
    <?php } ?>


    <form id="createForm" action="profile.php" method="POST">

        <p>Username: </p><input type=text id=username name=username value="<?php echo htmlspecialchars($name); ?>" placeholder="Username"><br />

        <p>Password: </p><input type=password id=password name="password" placeholder=Password value="<?php echo htmlspecialchars(""); ?>"><br>

        <p>E-mail: </p><input type=text id=email name=email placeholder=JohnDoe@hotmail.com value="<?php echo htmlspecialchars($email); ?>"><br>
        <p>City: </p><input type=text id=city name=city placeholder=City value="<?php echo htmlspecialchars($city); ?>"><br>
        <p>Birthday </p><input type=text id=bday name=bday placeholder=01-01-1970 value="<?php echo htmlspecialchars($bday); ?>"><br>

        <p>Phone number: </p><input type=text id=pnumber name=pnumber placeholder=0612345678 value="<?php echo htmlspecialchars($pnumber); ?>"><br>
        <p>Education: </p><input type=text id=education name=education placeholder=Education value="<?php echo htmlspecialchars($education); ?>"><br>


        <button name="submit" type="submit" class="btn btn-success">Register</button>


    </form>

</div>



<?php
require_once('footer.php');
?>
