<?php
require_once('config.inc.php');
require_once('header.php');


$query = $dbh->prepare(
    'SELECT * FROM users WHERE userId = ?'
);
$query->execute(array($_GET['userId']));
$results = $query->fetchAll();

foreach ($results as $row) {
    ?>
    <h1><?php echo htmlspecialchars($row['Username']); ?></h1>


    <table class="table">
        <tbody>
        <tr>
            <td>Username:</td>
            <td><?php echo htmlspecialchars($row['Username']); ?></td>
        </tr>
        <tr>
            <td>Email:</td>
            <td><?php echo htmlspecialchars($row['Email']); ?></td>
        </tr>
        <tr>
            <td>Birthday:</td>
            <td><?php echo htmlspecialchars($row['bday']); ?></td>
        </tr>
        <tr>
            <td>City:</td>
            <td><?php echo htmlspecialchars($row['city']); ?></td>
        </tr>
        <tr>
            <td>Education:</td>
            <td><?php echo htmlspecialchars($row['education']); ?></td>
        </tr>
        </tbody>
    </table>

    <?php
}
?>







<?php
require_once('footer.php');
?>
